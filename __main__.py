#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import math
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter


class Example(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()
        self.x1 = -3
        self.x2 = 3
        self.y1 = -3
        self.y2 = 3
        self.n = 50  # количество линий
        self.minx = 10000
        self.maxx = -self.minx
        self.miny = self.minx
        self.maxy = self.maxx

    def initUI(self):
        self.setWindowTitle('Surface')
        self.show()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.draw(qp)
        qp.end()

    #  отрисовываемые функции

    @staticmethod
    def func(x, y):
        return math.sin(x) * y

    @staticmethod
    def func1(x, y):
        return math.cos(x*y)

    @staticmethod
    def func2(x, y):
        return x*x

    @staticmethod
    def func3(x, y):
        return math.pow(math.sin(x), 2)*math.pow(math.cos(x), 2)

    # изометрия

    @staticmethod
    def coord_x(x, y):
        return (y-x)*math.sqrt(3)/2

    @staticmethod
    def coord_y(x, y, z):
        return (x+y)/2 - z

    #  алгоритм

    def draw(self, qp):
        # размеры окна
        mx = self.size().width()
        my = self.size().height()
        m = mx * 2
        top = [my for _ in range(mx)]
        bottom = [0 for _ in range(mx)]
        #  вычисление диапазона изменения координат
        for i in range(self.n+1):
            x = self.x2+i*(self.x1-self.x2)/self.n
            for j in range(self.n+1):
                y = self.y2+j*(self.y1-self.y2)/self.n
                z = self.func(x, y)    # отрисовываемая функция
                xx = self.coord_x(x, y)
                yy = self.coord_y(x, y, z)
                if xx > self.maxx:
                    self.maxx = xx
                if yy > self.maxy:
                    self.maxy = yy
                if xx < self.minx:
                    self.minx = xx
                if yy < self.miny:
                    self.miny = yy
        # алгоритм плавающего горизонта для удаления невидимых линий
        for i in range(self.n+1):
            x = self.x2+i*(self.x1-self.x2)/self.n
            for j in range(m+1):
                y = self.y2+j*(self.y1-self.y2)/m
                z = self.func(x, y)  # отрисовываемая функция
                xx = self.coord_x(x, y)
                yy = self.coord_y(x, y, z)
                xx = int((xx-self.minx)/(self.maxx-self.minx)*mx)
                yy = int((yy - self.miny) / (self.maxy - self.miny) * my)
                if yy > bottom[xx-1]:
                    qp.drawPoint(xx, yy)
                    bottom[xx-1] = yy
                if yy < top[xx-1]:
                    qp.drawPoint(xx, yy)
                    top[xx-1] = yy


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
